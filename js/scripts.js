(function($) {

    $(document).ready(function() {




        const KEY = '27024083-ebdc94bbdda3e3bb711d789f9';
        $(".contact_form form").submit(function(e) {
            e.preventDefault();
            let myWord = document.getElementById('myInput').value
            let values = {};
            $(this).serializeArray().map(function(x) {
                values[x.name] = x.value;
            });
            let form = $(this);

            //console.log(this)
            // $('.form_response').hide()

            $.get("https://pixabay.com/api/", { key: KEY, editors_choice: values.articles }).done(function(data) {
                console.log(data)
                if (data.hits && data.hits.length > 0) {
                    let allArticles = data.hits
                    let firstArticol = data.hits[0];
                    $('.popular').show()
                    $('.form_response img').attr('src', firstArticol.largeImageURL)
                    $('.form_response h2').html(firstArticol.views);
                    $('.form_response  p').html(firstArticol.tags);
                    $('.form_response .name').html(firstArticol.user)
                    $('.form_response .myId').html(firstArticol.likes + firstArticol.comments)
                    let secondRow = allArticles.slice(1, 4);
                    console.log(secondRow)
                    let data1 = '';
                    secondRow.map((values) => {
                        data1 += `<div data=${values.id} class="col-lg-4 col-sm12 mb-5 card">
                        <img class="secondRowImg" src=${values.largeImageURL} alt="">
                        <h3>${values.views}</h3>
                        <p>${values.tags}</p>
                        <div class="socials">
                            <div class="names">
                                <h6 class="nameArt">${values.user}</h6>
                                <h6 class="dateArt">${values.likes}  /  ${values.comments}</h6>
                            </div>
                            <div class="shares-btn">
                                <i class="fa-solid fa-bookmark bookMark"></i>
                                <i class="fa-solid fa-share-nodes"></i>
                            </div>
                        </div>
                    </div>`
                    })
                    document.getElementById('secondRow').innerHTML = data1

                    $('.seeMore p').click(function() {
                        $('hr').hide()
                        let thirdRow = allArticles.slice(3, 7);
                        console.log(thirdRow, 'third row')
                        let data2 = '';
                        thirdRow.map((values) => {
                            data2 += `<div data=${values.id} class="col-lg-3 col-sm-6 mb-5 card">
                            <img class="secondRowImg" src=${values.largeImageURL} alt="">
                            <h3>${values.views}</h3>
                            <p>${values.tags} </p>
                            <div class="socials">
                                <div class="names">
                                    <h6 class="nameArt">${values.user}</h6>
                                    <h6 class="dateArt">${values.likes}  /  ${values.comments}</h6>
                                </div>
                                <div class="shares-btn">
                                    <i  class="fa-solid fa-bookmark bookMark"></i>
                                    <i class="fa-solid fa-share-nodes"></i>
                                </div>
                            </div>
                        </div>`
                        })
                        document.getElementById('thirdRow').innerHTML = data2;
                        $(this).hide()





                        let fourdRow = allArticles.slice(8);
                        console.log(fourdRow, 'four row')
                        let data3 = '';
                        fourdRow.map((values) => {
                            data3 += `<div data=${values.id} class="col-lg-2 col-sm-6 mb-5 card">
                            <img class="secondRowImg" src=${values.previewURL} alt="">
                            <h3>${values.views}</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                            <div class="socials">
                                <div class="names">
                                    <h6 class="nameArt">${values.user}</h6>
                                    <h6 class="dateArt">${values.likes}  /  ${values.comments}</h6>
                                </div>
                                <div class="shares-btn">
                                    <i class="fa-solid fa-bookmark bookMark"></i>
                                    <i class="fa-solid fa-share-nodes"></i>
                                </div>
                            </div>
                        </div>`
                        })
                        document.getElementById('fourRow').innerHTML = data3;


                        ///modal
                        $('.card').click(function() {
                            console.log(this, 'acesta este thisu img')
                            const myId = $(this).attr('data');
                            $('.fa-bookmark').addClass('bookMark');
                            var modal = document.getElementById("myModal");
                            var span = document.getElementsByClassName("close")[0];

                            span.onclick = function() {
                                modal.style.display = "none";
                            }
                            window.onclick = function(event) {
                                if (event.target == modal) {
                                    modal.style.display = "none";
                                }
                            }
                            $(this).click(function() {
                                $(modal).show()
                            })

                            let curentArt = allArticles.filter(obj => {
                                return obj.id == myId
                            })

                            let [objSimple] = curentArt;


                            let index1
                            allArticles.forEach((article, index) => {
                                if (article === objSimple) index1 = index

                            });

                            $('#nextBtn').click(function() {
                                afisare(index1 += 1)
                            })
                            $('#prevBtn').click(function() {
                                afisare(index1 -= 1)
                            })

                            afisare(index1)
                        })

                        const afisare = function(index1) {

                            $('.modal-content img').attr('src', allArticles[index1].largeImageURL)
                            $('.modal-content h3').html(allArticles[index1].views);
                            $('.modal-content  p').html(allArticles[index1].tags);
                            $('.modal-content .nameArt').html(allArticles[index1].user)
                            $('.modal-content .dateArt').html(allArticles[index1].likes + allArticles[index1].comments);
                        }
                        var URL = "https://pixabay.com/api/?key=" + KEY + "&order=latest";
                        $.getJSON(URL, function(data) {
                            if (parseInt(data.totalHits) > 0) {
                                let latestAll = data.hits
                                let firstPage = latestAll.slice(0, 5);
                                let secondPage = latestAll.slice(4, 9);
                                let thirdPage = latestAll.slice(9, 14);
                                let fourPage = latestAll.slice(15);
                                console.log(secondPage, '2newww apicall');
                                console.log(thirdPage, '3newww apicall');
                                console.log(fourPage, '4newww apicall');
                                let page1 = '';
                                firstPage.map((values) => {
                                    page1 +=
                                        `<div class="col-4 card">
                        <img src="${values.largeImageURL}" alt="">
                    </div>
                    <div class="col-8 alignn card">
                        <h3>${values.views}</h3>
                        <p>${values.tags}</p>
                        <div class="socials">
                            <div class="names">
                                <h6 class="nameArt">${values.user}</h6>
                                <h6 class="dateArt">${values.likes} / ${values.comments}</h6>
                            </div>
                            <div class="shares-btn">
                                <i class="fa-solid fa-bookmark bookMark"></i>
                                <i class="fa-solid fa-share-nodes"></i>
                            </div>
                        </div>
                    </div>`
                                })
                                let page2 = '';
                                secondPage.map((values) => {
                                    page2 +=
                                        `<div class="col-4 card">
                        <img src="${values.largeImageURL}" alt="">
                    </div>
                    <div class="col-8 alignn card">
                        <h3>${values.views}</h3>
                        <p>${values.tags}</p>
                        <div class="socials">
                            <div class="names">
                                <h6 class="nameArt">${values.user}</h6>
                                <h6 class="dateArt">${values.likes} / ${values.comments}</h6>
                            </div>
                            <div class="shares-btn">
                                <i class="fa-solid fa-bookmark bookMark"></i>
                                <i class="fa-solid fa-share-nodes"></i>
                            </div>
                        </div>
                    </div>`
                                })

                                let page3 = '';
                                thirdPage.map((values) => {
                                    page3 +=
                                        `<div class="col-4 card">
                        <img src="${values.largeImageURL}" alt="">
                    </div>
                    <div class="col-8 alignn card">
                        <h3>${values.views}</h3>
                        <p>${values.tags}</p>
                        <div class="socials">
                            <div class="names">
                                <h6 class="nameArt">${values.user}</h6>
                                <h6 class="dateArt">${values.likes} / ${values.comments}</h6>
                            </div>
                            <div class="shares-btn">
                                <i class="fa-solid fa-bookmark bookMark"></i>
                                <i class="fa-solid fa-share-nodes"></i>
                            </div>
                        </div>
                    </div>`
                                })
                                document.getElementById('listing').innerHTML = page3

                                let page4 = '';
                                fourPage.map((values) => {
                                    page4 +=
                                        `<div class="col-4  card">
                            <img src="${values.largeImageURL}" alt="">
                        </div>
                        <div class="col-8 alignn card">
                            <h3>${values.views}</h3>
                            <p>${values.tags}</p>
                            <div class="socials">
                                <div class="names">
                                    <h6 class="nameArt">${values.user}</h6>
                                    <h6 class="dateArt">${values.likes} / ${values.comments}</h6>
                                </div>
                                <div class="shares-btn">
                                    <i class="fa-solid fa-bookmark bookMark"></i>
                                    <i class="fa-solid fa-share-nodes"></i>
                                </div>
                            </div>
                        </div>`
                                })



                                allPages = [page1, page2, page3, page4]
                                document.getElementById('listing').innerHTML = allPages[0];
                                let curentPageIndex = allPages.indexOf(page1)
                                console.log(curentPageIndex, 'indexul meu')

                                console.log(firstPage, secondPage, thirdPage, fourPage, 'toate paginoile')

                                $('#next').click(function() {
                                    let NextPAge = curentPageIndex;
                                    if (NextPAge < allPages.length && curentPageIndex !== allPages.length - 1) {
                                        NextPAge = curentPageIndex += 1
                                        document.getElementById('listing').innerHTML = allPages[NextPAge];
                                        console.log(NextPAge, 'a intrat')
                                    }

                                })
                                $('#prev').click(function() {
                                    let prevPAge = curentPageIndex;
                                    if (prevPAge >= 0 && curentPageIndex !== 0) {
                                        prevPAge = curentPageIndex -= 1
                                        document.getElementById('listing').innerHTML = allPages[prevPAge];
                                        console.log('a intrat peverso')
                                    }
                                })
                            } else
                                console.log('No hits');
                            //favorites
                            let favorites = [];
                            $('.shares-btn .bookMark').on("click", function() {
                                $(this).toggleClass('fav')
                                console.log(this, 'thisu')
                            })
                        });

                    })
                }
            });




            //latest 

















        });








    });



})(jQuery);